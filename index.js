// const express = require('express')
// const app = express()

require('dotenv').config()

// app.use(express.json())
// app.use(require('morgan')('dev'))

// app.get('/', (req, res, next) => {
// })

const sql = require('mssql');
(async () => {
    try {
        // make sure that any items are correctly URL encoded in the connection string
        const connectionString = process.env.DB_CONNECTION_STRING
            .replace('${DB_PROVIDER}', process.env.DB_PROVIDER)
            .replace('${DB_HOST}', process.env.DB_HOST)
            .replace('${DB_USERNAME}', process.env.DB_USERNAME)
            .replace('${DB_PASSWORD}', process.env.DB_PASSWORD)
            .replace('${DB_DATABASE}', process.env.DB_DATABASE)
        console.log({connectionString})
        await sql.connect(connectionString)
        
        //const result = await sql.query`SELECT * FROM View_ALI` //  where id = ${value}
	//const result2 = await sql.query`SELECT * FROM View_Cellphone_location_latest`
        //console.log({rs_View_ALI: result.recordset[0], rs_View_Cellphone_location_latest: result2.recordset[0]})

	const result = await sql.query(process.env.TEST_SQL)
	console.log({rs: JSON.stringify(result.recordset)})

	require('fs').writeFile('./rs.json', JSON.stringify(result), ()=>{})

    } catch (err) {
        console.log({err})
        // ... error checks
    }
})()

// app.listen(7600, () => {
//     console.log('http://localhost:7600')
// })
